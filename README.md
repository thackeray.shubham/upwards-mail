# UpwardsGmail

note: Its very difficult to make this DOC as it turns out to be more time consuming.
      I hope you also believe in saving time just liek I do.

      I like to keep code as simple as it can be humanly, and thus in order to keep code simple and save time I havent used any stateManagement system or Subjects. 
      
      

<!-- component schema  -->

level 1
login compoent
landing compnent
auth-guard-service
auth-interceptor
gauth-service
api-service
api-base-service

level2
history-bar-graph
history-pie-graph
header-component
mail-label-table-component

1> landing component - consist of 3 main eagerloaded tabs and rest tabs are lazy loaded for each label from google api services

// function to get all available mail labels from google api and reflect it to DOM
getLabels()

2> login compoent
consist a login button to call login function from auth service
we have used gapi-plugin in order to save developement time

3> auth-guard-service
set up auth-guard for the routing returns the value true/false for valid route.

4>auth-interceptor
checks for the email error status 401.
if triggered make a logout call

5> gauth-service
// function to getUserToken
public getToken()

// function to get user Id
public getUserId()

// function to signIn using GAPI
public signIn()

    // function to logout and erase data on session storage
      logout()

<!-- assignment phase 1 -->

1> mail-label-table component
input label

// Function to fetch array of mail ids for required page from google apis
getMailIds(label:string)

// function to get mails for each mail id and add to DOM
getmails(mailIds)

// function page change to change the page to required
pageChange(event)

note: data size cant be determined and thats why the table size is hardcoded to 200

  <!-- assignment phase 2 -->

Graphs:
1>history-bar-graph-copmonent

// we have a recursive function call here to calculate number of elements
// we have a progressive loader approach as we might be hitting multiple api calls

getMailData(start, end, monthIndex, nextPageToken?)

// function to calculate starting and ending date of ech month for 12 months before
// and to make a chartData object
getMonths()

// function to hide loader
hideLoading()

2>history-pie-graph-component

 //function to make recursive api calls to get count  
  getMailData(start, end, labelIndex, nextPageToken?,lableId?)

  // function to create chartData
  getLabelData() 

 <!-- assignment phase 3 -->
header-component

// function to send email reply to thread
  sendThread(threadId);

 // function being called on buttin click to fetch the filtered threads and if the
  // recent thread doesnt have last mail same as scripted it will call function to send the mail   
  getThread() 

// function to encode hardcoded mail
  getMailData() 

 // function to open email modal
  openModal()



         


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.15.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
