import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  GoogleApiModule, 
  GoogleApiService, 
  GoogleAuthService, 
  NgGapiClientConfig, 
  NG_GAPI_CONFIG,
  GoogleApiConfig
} from "ng-gapi";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AuthGuard } from './services/auth-guard.service';
import { GauthService } from './services/gauth.service';

let gapiClientConfig: NgGapiClientConfig = {
  client_id: "848882469177-gp267pi237vvkntdf6ojhn01efdtq1uj.apps.googleusercontent.com",
  discoveryDocs: ["https://analyticsreporting.googleapis.com/$discovery/rest?version=v4"],
  scope: [
      "https://www.googleapis.com/auth/gmail.readonly",
      "https://www.googleapis.com/auth/gmail.addons.current.message.readonly",
      "https://www.googleapis.com/auth/gmail.send"
  ].join(" ")
};
import {MatTabsModule} from '@angular/material/tabs';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GAuthInterceptor } from './services/gauth.interceptor';
import { MailLabelTableComponent } from './components/mail-label-table/mail-label-table.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { HistoryPieGraphComponent } from './components/graphs/history-pie-graph/history-pie-graph.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HistoryBarGraphComponent } from './components/graphs/history-bar-graph/history-bar-graph.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { HeaderUpwardsComponent } from './components/header-upwards/header-upwards.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    LandingPageComponent,
    MailLabelTableComponent,
    HistoryBarGraphComponent,
    HistoryPieGraphComponent,
    HeaderUpwardsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
    BrowserAnimationsModule,
    NgbModule,
    MatTabsModule,
    MatPaginatorModule,
    NgxChartsModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
  ],
  providers: [AuthGuard,GauthService,
    { provide: HTTP_INTERCEPTORS, useClass: GAuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
