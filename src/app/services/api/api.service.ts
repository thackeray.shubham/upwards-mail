import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiBaseService } from '../api-base/api-base.service';
import { GauthService } from '../gauth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  constructor(private api: ApiBaseService,private auth:GauthService) {}


  getMailIds(label?:any,pageToken?:any) {
    let user = this.auth.getUserId();
    console.log(user);
    if(label  && !pageToken){
    return this.api.get('users/'+user+'/messages?labelIds='+label, {}, {});
  }else if(!label && !pageToken){
    return this.api.get('users/'+user+'/messages', {}, {});
  }else if(label && pageToken){
    return this.api.get('users/'+user+'/messages?labelIds='+label+'&pageToken='+pageToken, {}, {});
  }else{
    return this.api.get('users/'+user+'/messages?pageToken='+pageToken, {}, {});
  }
}

getMailHistory(start:number,end:number,pageToken?:number,labelId?:string){
  let user = this.auth.getUserId();
  // after:1388552400 before:1391230800

if(pageToken && !labelId){
  return this.api.get('users/'+user+'/messages?pageToken='+pageToken+'&q=after:'+start+' before:'+end, {}, {});
}else if(!pageToken && !labelId){
  return this.api.get('users/'+user+'/messages?q=after:'+start+' before:'+end, {}, {});
}else if(pageToken && labelId){
  return this.api.get('users/'+user+'/messages?labelIds='+labelId+'&pageToken='+pageToken+'&q=after:'+start+' before:'+end, {}, {});
}else if(!pageToken && labelId){
  return this.api.get('users/'+user+'/messages?labelIds='+labelId+'&q=after:'+start+' before:'+end, {}, {});
}
  // return this.api.get('users/'+user+'/messages?q=after:1388552400 before:1391230800', {}, {});

}
  getMails(id) {
    let user = this.auth.getUserId();
    console.log(user);
    return this.api.get('users/'+user+'/messages/'+id, {}, {});
  }

 getLabels() {
    let user = this.auth.getUserId();
    console.log(user);
    return this.api.get('users/'+user+'/labels', {}, {});
  }
  sendMail(raw,threadId?){
    let user = this.auth.getUserId();
    return this.api.post('users/'+user+'/messages/send', {raw:raw,threadId:threadId}, {});
  }
  getThreads(filterFrom){
    let user = this.auth.getUserId();
    return this.api.get('users/'+user+'/threads?q=from:'+filterFrom, {}, {});
  }
  getThread(id){
    let user = this.auth.getUserId();
    return this.api.get('users/'+user+'/threads/'+id, {}, {});
  }
}
