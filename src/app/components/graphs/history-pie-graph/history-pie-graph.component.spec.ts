import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryPieGraphComponent } from './history-pie-graph.component';

describe('HistoryPieGraphComponent', () => {
  let component: HistoryPieGraphComponent;
  let fixture: ComponentFixture<HistoryPieGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryPieGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryPieGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
