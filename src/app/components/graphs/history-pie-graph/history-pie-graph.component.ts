import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { chartData } from '../history-bar-graph/history-bar-graph.component';

@Component({
  selector: 'app-history-pie-graph',
  templateUrl: './history-pie-graph.component.html',
  styleUrls: ['./history-pie-graph.component.scss']
})
export class HistoryPieGraphComponent implements OnInit {
  @Input() labels;

  chartData:chartData[] = [];
  single: any[];
  view: any[] = [700, 400];
  // loading = 0 ;
  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';
   
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private api:ApiService) {
    // Object.assign(this, { single })
  }
  getMailData(start, end, labelIndex, nextPageToken?,lableId?) {
    // let pageTokens = [];
    if (nextPageToken) {
      this.api.getMailHistory(start, end, nextPageToken,lableId).subscribe((res) => {
        if (res?.messages) {
          // this.hideLoading();
          this.chartData[labelIndex].value =
            this.chartData[labelIndex]?.value + res?.messages?.length;
          // console.log(this.chartData[labelIndex]);
          if (res.nextPageToken) {
            // pageTokens.push(res.nextPageToken);
            this.getMailData(start, end, labelIndex, res.nextPageToken , lableId);
          } else if (!res.nextPageToken) {
            // this.hideLoading();
            this.single = JSON.parse(JSON.stringify(this.chartData));
          }
        }
      });
    } else if (!nextPageToken) {
      this.api.getMailHistory(start, end , undefined , lableId).subscribe((res) => {
        if (res?.messages) {
          // console.log(this.chartData);
          this.chartData[labelIndex].value =
            this.chartData[labelIndex]?.value + res?.messages?.length;
          if (res.nextPageToken) {
            // pageTokens.push(res.nextPageToken);
            this.getMailData(start, end, labelIndex, res.nextPageToken,lableId);
          } else if (!res.nextPageToken) {
            // this.hideLoading();
            this.single = JSON.parse(JSON.stringify(this.chartData));
          }
        } 
      });
    }
  }

  onSelect(data): void {
    // console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    // console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    // console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  ngOnInit(): void {
    // console.log(this.labels);
    this.getLabelData();
  }

  getLabelData() {
    var date = new Date();
    for (let i = 0; i < 4; i++) {
      var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() , 0);
      // console.log(firstDay.getTime() / 1000, lastDay);
      this.chartData.push({
        name: this.labels[i].id,
        value: 0,
      });
      this.getMailData(firstDay.getTime() / 1000, lastDay.getTime() / 1000, i, null ,this.labels[i].id);
    }
  }

 
}
