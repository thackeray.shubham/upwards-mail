import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryBarGraphComponent } from './history-bar-graph.component';

describe('HistoryBarGraphComponent', () => {
  let component: HistoryBarGraphComponent;
  let fixture: ComponentFixture<HistoryBarGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryBarGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryBarGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
