import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

export interface chartData {
  name: string;
  value: number;
}
@Component({
  selector: 'app-history-bar-graph',
  templateUrl: './history-bar-graph.component.html',
  styleUrls: ['./history-bar-graph.component.scss'],
})
export class HistoryBarGraphComponent implements OnInit {
  single: any[];
  multi: any[];

  view: any[] = [700, 400];

  // Chart options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  };

  monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  chartData: chartData[] = [];
  loading = 0;

  constructor(private api: ApiService) {}

  ngOnInit(): void {
    this.getMonths();
  }

  // function getMailData()
  //  we have a recursive function call here to calculate number of elements
  //  we have a progressive loader approach as we might be hitting multiple api calls

  getMailData(start, end, monthIndex, nextPageToken?) {
    // let pageTokens = [];
    if (nextPageToken) {
      this.api.getMailHistory(start, end, nextPageToken).subscribe((res) => {
        if (res?.messages) {
          // this.hideLoading();
          this.chartData[monthIndex].value =
            this.chartData[monthIndex]?.value + res?.messages?.length;
          // console.log(this.chartData[monthIndex]);
          if (res.nextPageToken) {
            // pageTokens.push(res.nextPageToken);
            this.getMailData(start, end, monthIndex, res.nextPageToken);
          } else if (!res.nextPageToken) {
            this.hideLoading();
            this.single = JSON.parse(JSON.stringify(this.chartData));
          }
        } else {
          this.hideLoading();
        }
      });
    } else if (!nextPageToken) {
      this.api.getMailHistory(start, end).subscribe((res) => {
        if (res?.messages) {
          // console.log(this.chartData);
          this.chartData[monthIndex].value =
            this.chartData[monthIndex]?.value + res?.messages?.length;
          if (res.nextPageToken) {
            this.loading = this.loading + 1;
            // pageTokens.push(res.nextPageToken);
            this.getMailData(start, end, monthIndex, res.nextPageToken);
          } else if (!res.nextPageToken) {
            this.hideLoading();
            this.single = JSON.parse(JSON.stringify(this.chartData));
          }
        } else {
          this.hideLoading();
        }
      });
    }
  }

 
// TODO --functionality on demand
//  function for chart onPieSelect event 
  onSelect(event) {
    // console.log(event);
  }

//  function to calculate starting and ending date of ech month for 12 months before
//  and to make a chartData object
  getMonths() {
    var date = new Date();
    for (let i = 0; i < 12; i++) {
      var firstDay = new Date(date.getFullYear(), date.getMonth() - i - 1, 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() - i, 0);
      // console.log(firstDay.getTime() / 1000, lastDay);
      this.chartData.push({
        name: this.monthNames[firstDay.getMonth()],
        value: 0,
      });
      this.getMailData(firstDay.getTime() / 1000, lastDay.getTime() / 1000, i);
    }
  }

  // function to hide loader 
  hideLoading() {
    this.loading = this.loading - 1;
    // console.log(this.loading);
  }
}
