import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

export interface label {
  id: any;
  labelListVisibility?: any;
  messageListVisibility?: any;
  name: any;
  type?: any;
}

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  labels: label[] = [];
  constructor(private api: ApiService) {}

  ngOnInit(): void {
    this.getLabels();
  }

  // function to get the mail labels from google and reflect it to DOM
  getLabels() {
    this.api.getLabels().subscribe((res) => {
      if (res?.labels) {
        this.labels = res.labels;
      }
    });
  }
}
