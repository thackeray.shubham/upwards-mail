import { Component, OnInit } from '@angular/core';
import { GauthService } from 'src/app/services/gauth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(private auth:GauthService) { }

  ngOnInit(): void {
  }
  // function to proceed to login window
  login(){
    this.auth.signIn();
  }
}
