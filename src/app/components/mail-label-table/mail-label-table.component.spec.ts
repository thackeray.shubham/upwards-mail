import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailLabelTableComponent } from './mail-label-table.component';

describe('MailLabelTableComponent', () => {
  let component: MailLabelTableComponent;
  let fixture: ComponentFixture<MailLabelTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailLabelTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailLabelTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
