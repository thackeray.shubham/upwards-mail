import { Component, Input, NgZone, OnInit } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';

export interface mail {
  subject: any;
  body: string;
  from: any;
  recipients: any;
  recieved_datetime: any;
}

@Component({
  selector: 'app-mail-label-table',
  templateUrl: './mail-label-table.component.html',
  styleUrls: ['./mail-label-table.component.scss'],
})
export class MailLabelTableComponent implements OnInit {
  @Input() label;
  constructor(private api: ApiService, private zone: NgZone) {}
  mailIds: any;
  mailPageTokens = [];
  mails: mail[] = [];
  pageIndex = 0;
  loader:boolean = false;

  //  hardcoded tableSize
  resultSizeEstimated: number = 200;

  ngOnInit(): void {
    this.getMailIds(this.label);
  }

  // Function to fetch array of mail ids for required page from google apis
  getMailIds(label: string) {
    if (this.pageIndex && this.mailPageTokens[this.pageIndex - 1]) {
      this.api
        .getMailIds(label, this.mailPageTokens[this.pageIndex])
        .subscribe((res) => {
          console.log(res);
          this.mailIds = res?.messages;
          this.mailPageTokens.push(res?.nextPageToken);
          this.getmails(res?.messages);
          // this.resultSizeEstimated= res?.resultSizeEstimate;
        });
    } else if (this.pageIndex == 0) {
      this.api.getMailIds(label).subscribe((res) => {
        console.log(res);
        this.mailIds = res?.messages;
        this.mailPageTokens.push(res?.nextPageToken);
        this.getmails(res?.messages);
        // this.resultSizeEstimated= res?.resultSizeEstimate;
      });
    }
  }

  // function to get mails for each mail id and add to DOM
  getmails(mailIds) {
    this.loader = true ; 
    this.getData(mailIds).subscribe((res) => {
      this.mails = this.setMails(res);
      this.loader =false;
    });
  }

  // function page change to change the page to required
  pageChange(e) {
    console.log(e);
    this.pageIndex = e.pageIndex;
    this.getMailIds(this.label);
  }

  // setMails(mails:mail[]){
  //   this.mails = mails;
  // }

  getData(data): Observable<any> {
    let response = [];
    data.forEach((item, index) => {
      response[index] = this.api.getMails(item?.id);
    });
    return forkJoin([...response]);
  }

  setMails(data) {
    let mails = [];
    data.forEach((res) => {
      let mail: mail = {
        subject: res.payload.headers.filter((val) => val.name == 'Subject')[0]
          ?.value,
        body: res.snippet,
        from: res.payload.headers.filter((val) => val.name == 'From'),
        recipients: res.payload.headers.filter((val) => val.name == 'To'),
        recieved_datetime: res.payload.headers.filter(
          (val) => val.name == 'Date'
        ),
      };
      mails.push(mail);
    });
    return mails;
  }
}
