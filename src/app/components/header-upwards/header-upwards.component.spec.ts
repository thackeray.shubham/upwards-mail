import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderUpwardsComponent } from './header-upwards.component';

describe('HeaderUpwardsComponent', () => {
  let component: HeaderUpwardsComponent;
  let fixture: ComponentFixture<HeaderUpwardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderUpwardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderUpwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
