import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api/api.service';
import { GauthService } from 'src/app/services/gauth.service';

@Component({
  selector: 'app-header-upwards',
  templateUrl: './header-upwards.component.html',
  styleUrls: ['./header-upwards.component.scss'],
})
export class HeaderUpwardsComponent implements OnInit {
  mailBody: string = 'Hello There, We will get back to you on this.';

  // TODO add other filter options to form if asked
  form: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required]],
  });

  constructor(
    private formBuilder: FormBuilder,
    private auth: GauthService,
    private api: ApiService,
    private toastr: ToastrService,
    private modalService: NgbModal,
  ) {}

  ngOnInit(): void {}


  sendThread(threadId) {
    this.api.sendMail(this.getMailData(), threadId).subscribe((res) => {
      this.toastr.success('Mail Sent', 'replied successfully on the thread!');
    });
  }

  // function being called on buttin click to fetch the filtered threads and if the
  // recent thread doesnt have last mail same as scripted it will call function to send the mail   
  getThread() {
    // TODO make  a thread select ui if required
    this.api.getThreads(this.form.value.email).subscribe((res) => {
      console.log(res);
      if (res.threads[0].id) {
        this.api.getThread(res.threads[0]?.id).subscribe((res) => {
          console.log(res);
          if (
            res?.messages[res?.messages.length - 1]?.snippet !== this.mailBody
          ) {
            this.sendThread(res?.id);
          } else {
            this.toastr.success(
              'Already Sent',
              'This thread was already responded!'
            );
          }
        });
      } else {
        this.toastr.error(
          'Thread not found',
          'thread for given filter not found ask thackeray.shubham@gmail.com to inititate thread!'
        );
      }
    });
  }

  // function to encode hardcoded mail
  getMailData() {
    const User = this.auth.getUserId;
    const subject = 'Hi';
    const To = this.form.value.email;

    const mailData = [
      'From:' + User,
      'To:'+To,
      'Subject: =?utf-8?B?' +
        window.btoa(unescape(encodeURIComponent(subject))) +
        '?=',
      'MIME-Version: 1.0',
      'Content-Type: text/plain; charset=UTF-8',
      'Content-Transfer-Encoding: 7bit',
      '',
      this.mailBody,
    ]
      .join('\n')
      .trim();
    return window
      .btoa(unescape(encodeURIComponent(mailData)))
      .replace(/\+/g, '-')
      .replace(/\//g, '_');
  }

  // function to open email modal
  openModal(customContent){
    this.modalService.open(customContent, {
      windowClass: 'dark-modal',
      backdrop: 'static',
      size: 'md',
    });
  }
}
